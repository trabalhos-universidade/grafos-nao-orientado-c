/*Autor: Fabio Duarte de Souza
  Disciplina: Estrutura de dados II
  Solicitado pela professora Priscyla Waleska T. A. Sim�es
  Este algoritmo tem a finalidade de simular a contrucao de um grafo nao orientado
  
*/

#include "grafo.h"

int menu();

main()
{
    int op,i,noini,nofim,tam,vert;
    grafo *no = new grafo;
    list<vertice>::iterator auxvt;
    list<int>::iterator auxl;
    do
    {
        op = menu();
        switch(op)
        {
            case 1: if (!no->grafovazio())
                    {
                        delete no;
                        no = new grafo;
                        cout<<"\nGrafo criado.\n";
                    }
                    else
                        cout<<"\nGrafo vazio. Desnecessario cria-lo.\n";
                    system("pause");
            break;
                    
            case 2: cout<<"\nInsira o numero do vertice: ";
                    cin>>vert;
                    if (!no->existevertice(vert))
                    {
                        no->inserirvertice(vert);
                        cout<<"\nVertice inserido com sucesso\n";
                    }
                    else
                        cout<<"\nNao sao admitidos vertices duplicados.\n";
                    system("pause");
            break;
            
            case 3: if (!no->grafovazio())
                    {
                        cout<<"\nVertices ja inseridos no grafo:\n\n";
            
                        no->consultavertices();
                        
                        cout<<"\nDigite o vertice inicial: ";
                        cin>>noini;
                        cout<<"\nDigite o vertice final: ";
                        cin>>nofim;
                        
                        no->inseriraresta(noini,nofim);
                    }
                    else
                        cout<<"\nGrafo vazio.\n";
                    system("pause");
            break;
            
            case 4: if (!no->grafovazio())
                    {
                        cout<<"\nLista de adjacencias\n\n";
                        no->consultargrafo();
                    }
                    else
                        cout<<"\nGrafo vazio.\n";
                    system("pause");
            break;
            
            case 5: if (!no->grafovazio())
                    {
                        cout<<"\nInforme o vertice: ";
                        cin>>vert;
                        no->consultararestas(vert);
                    }
                    else
                        cout<<"\nGrafo vazio.\n";
                    system("pause");
            break;
            
            case 6: if (!no->grafovazio())
                    {
                        cout<<"\nInforme o vertice: ";
                        cin>>vert;
                        no->removervertice(vert);
                    }
                    else
                        cout<<"\nGrafo vazio.\n";
                    system("pause");
            break;
            
            case 7: if (!no->grafovazio())
                    {
                        cout<<"\nDigite o vertice inicial: ";
                        cin>>noini;
                        cout<<"\nDigite o vertice final: ";
                        cin>>nofim;

                        no->removeraresta(noini,nofim);
                    }
                    else
                        cout<<"\nGrafo vazio.\n";
                    system("pause");
            break;
            
        }
    } while (op > 0);
}

int menu() {
    int op=0;
    system("cls");
    cout<<"\n\nPratica 6 - Grafos representados por multilistas";
    cout<<"\n\n\n 1- Criar";
    cout<<"\n 2- Inserir vertices";
    cout<<"\n 3- Inserir arestas";
    cout<<"\n 4- Consultar grafo" ;
    cout<<"\n 5- Consultar arestas (de um vertice)";
    cout<<"\n 6- Remover vertice";
    cout<<"\n 7- Remover aresta";
    cout<<"\n\n 0- Sair";
    cout<<"\n\nDigite a opcao: ";
    cin>>op;
    return op;
}
