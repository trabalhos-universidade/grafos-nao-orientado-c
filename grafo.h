#include "funcoes.h"

class vertice
{
    private:
        list<int> l;
        int rotulo;
        
    public:
        vertice();
        vertice(int rotulo);
        ~vertice();
        int getrotulo();
        void setrotulo(int rotulo);
        bool consultavertice();
        void getaresta(int vert, list<int>::iterator &auxl);
        void inserearesta(int vert);
        bool removearesta(int vert);
};

class grafo
{
    private:
        list<vertice> no;
        
    public:
        grafo();
        ~grafo();
        bool grafovazio();
        void inserirvertice(int rotulo);
        int gettamanho();
        void getvertice(int vertice, list<vertice>::iterator &auxvt);
        void inseriraresta(int inicial, int final);
        void consultargrafo();
        void consultavertices();
        void consultararestas(int vert);
        void removervertice(int vert);
        void removeraresta(int inicio, int final);
        bool existevertice(int vert);
};
