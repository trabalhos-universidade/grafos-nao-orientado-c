#include "grafo.h"

vertice::vertice()
{

}

vertice::vertice(int rotulo)
{
    this->rotulo = rotulo;
}

vertice::~vertice()
{
    l.clear();
}

int vertice::getrotulo()
{
    return rotulo;
}

void vertice::setrotulo(int rotulo)
{
    this->rotulo = rotulo;
}

bool vertice::consultavertice()
{
    bool status = false;
    list<int>::iterator auxl;
    for (auxl = l.begin();auxl != l.end();auxl++)
    {
        status = true;
        cout<<"\t->  "<<*auxl;
    }
    return status;
}

void vertice::getaresta(int vert, list<int>::iterator &auxl)
{
    auxl = find(l.begin(),l.end(),vert);
}


void vertice::inserearesta(int vert)
{
    l.push_back(vert);
}

bool vertice::removearesta(int vert)
{
    list<int>::iterator auxl;
    auxl = l.begin();
    getaresta(vert,auxl);
    if (auxl != l.end())
    {
        l.erase(auxl);
        return true;
    }
    return false;
}


//----------------------------------------------------------------------------------------


grafo::grafo()
{

}

grafo::~grafo()
{
    no.clear();
}


bool grafo::grafovazio()
{
    if (no.empty())
        return true;
    else
        return false;
}

void grafo::inserirvertice(int rotulo)
{
    vertice *vt = new vertice(rotulo);
    no.push_back(*vt);
}

int grafo::gettamanho()
{
    return no.size();
}

void grafo::inseriraresta(int inicio, int final)
{
    list<vertice>::iterator auxvt,auxvt2;
    
    auxvt = no.begin();
    while ((auxvt != no.end()) && (inicio != auxvt->getrotulo()))
        auxvt++;
    if (auxvt == no.end())
        cout<<"\nVertice nao encontrado\n";
    else
    {
        if (inicio != final)
        {
            auxvt2 = auxvt;
            auxvt = no.begin();
            while ((auxvt != no.end()) && (final != auxvt->getrotulo()))
                auxvt++;
            if (auxvt == no.end())
                cout<<"\nVertice nao encontrado\n";
            else
            {
                auxvt->inserearesta(inicio);
                auxvt2->inserearesta(final);
                cout<<"\nAresta inserida com sucesso\n";
            }
        }
        else
        {
           auxvt->inserearesta(inicio);
           cout<<"\nAresta inserida com sucesso\n";
        }
    }
}


void grafo::consultavertices()
{
    list<vertice>::iterator auxvt;
    auxvt = no.begin();
    for (auxvt == no.begin();auxvt != no.end();auxvt++)
        cout<<auxvt->getrotulo()<<"   ";
}

void grafo::consultargrafo()
{
    list<vertice>::iterator auxvt;
    
    cout<<"\nVertice Adjacencias\n";
    for (auxvt = no.begin();auxvt != no.end();auxvt++)
    {
        cout<<"\n   "<<auxvt->getrotulo();
        auxvt->consultavertice();
        cout<<endl<<endl;
    }
}

void grafo::consultararestas(int vert)
{
    list<vertice>::iterator auxvt;
    
    getvertice(vert,auxvt);
    if (auxvt == no.end())
        cout<<"\nVertice inexistente\n";
    else
    {
        cout<<"\nArestas do vertice: "<<auxvt->getrotulo()<<":";
        if (!auxvt->consultavertice())
            cout<<"\t\tArestas inexistentes";
    }
        cout<<endl<<endl;
}

void grafo::getvertice(int vertice, list<vertice>::iterator &auxvt)
{
    auxvt = no.begin();
    while ((auxvt != no.end()) && (vertice != auxvt->getrotulo()))
        auxvt++;
}

void grafo::removervertice(int vert)
{
    list<vertice>::iterator auxvt;

    auxvt = no.begin();
    
    //verifica se o vertice existe
    while ((auxvt != no.end()) && (vert != auxvt->getrotulo()))
        auxvt++;
    if (auxvt == no.end())
        cout<<"\nVertice nao encontrado\n";
    else
    {
        no.erase(auxvt);
        auxvt = no.begin();
        while (auxvt != no.end())
        {
            auxvt->removearesta(vert);
            auxvt++;
        }
        cout<<"\nVertice removido com sucesso.\n";
    }
}

void grafo::removeraresta(int inicio, int final)
{
    list<vertice>::iterator auxvt,auxvt2;

    auxvt = no.begin();
    while ((auxvt != no.end()) && (inicio != auxvt->getrotulo()))
        auxvt++;
    if (auxvt == no.end())
        cout<<"\nVertice nao encontrado\n";
    else
    {
        if (inicio != final)
        {
            auxvt2 = auxvt;
            auxvt = no.begin();
            while ((auxvt != no.end()) && (final != auxvt->getrotulo()))
                auxvt++;
            if (auxvt == no.end())
                cout<<"\nVertice nao encontrado\n";
            else
            {
                if (auxvt->removearesta(inicio))
                {
                    auxvt2->removearesta(final);
                    cout<<"\nAresta removida com sucesso\n";
                }
                else
                    cout<<"\nAresta nao encontrada\n";
            }
        }
        else
        {
            if (auxvt->removearesta(inicio))
                cout<<"\nAresta removida com sucesso\n";
            else
                cout<<"\nAresta nao encontrada\n";
        }
    }
}

bool grafo::existevertice(int vert)
{
    list<vertice>::iterator auxvt;
    
    auxvt = no.begin();
    while ((auxvt != no.end()) && (vert != auxvt->getrotulo()))
        auxvt++;
    if (auxvt != no.end())
        return true;
    return false;
}

